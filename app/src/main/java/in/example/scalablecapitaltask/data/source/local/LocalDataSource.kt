package `in`.example.scalablecapitaltask.data.source.local

import `in`.example.scalablecapitaltask.data.models.GithubRepository
import android.content.Context


class LocalDataSource(private val context: Context) {

    fun insertRepositories(repoList: List<GithubRepository>) {
        AppDatabase.getInstance(context).repositoryDao().insertRepositories(*repoList
                .toTypedArray())
    }

    fun getAllRepositories() =
        AppDatabase.getInstance(context).repositoryDao().getAllRepositories()

}