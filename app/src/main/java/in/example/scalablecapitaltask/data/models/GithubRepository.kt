package `in`.example.scalablecapitaltask.data.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "repositories")
class GithubRepository(@PrimaryKey val id: Long, val name: String)  {
    @Ignore var lastCommit: RepositoryCommit? = null
}