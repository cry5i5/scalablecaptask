package `in`.example.scalablecapitaltask.data.models


class RepositoryCommit(val message: String, val authorDisplayName: String, timestamp: Long)