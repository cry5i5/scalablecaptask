package `in`.example.scalablecapitaltask.data.models

import java.net.URL


enum class RequestType {
    repository {
        override fun getUrl(vararg params: String): URL {
            return URL("https://api.github.com/users/${params[0]}/repos?page=${params[1]}")
        }
    }, commit {
        override fun getUrl(vararg params: String): URL {
            return URL("https://api.github.com/repos/${params[0]}/${params[1]}/commits")
        }
    };

    abstract fun getUrl(vararg params: String): URL
}