package `in`.example.scalablecapitaltask.data.source.local

import `in`.example.scalablecapitaltask.data.models.GithubRepository
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface RepositoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepositories(vararg repos: GithubRepository)

    @Query("Select * from repositories")
    fun getAllRepositories(): List<GithubRepository>
}