package `in`.example.scalablecapitaltask.data.models

import org.json.JSONArray
import org.json.JSONException
import java.text.SimpleDateFormat


class CommitApiResponse(jsonString: String) {
    var lastCommit: RepositoryCommit? = null

    init {
        try {
            val jsonArray = JSONArray(jsonString)
            val lastCommitObj = jsonArray.getJSONObject(0)
            val commitObj = lastCommitObj.getJSONObject("commit")
            val msg = commitObj.getString("message")

            val authorObj = commitObj.getJSONObject("author")

            val displayName = authorObj.getString("name")
            val dateString = authorObj.getString("date")
            val timestamp = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(dateString).time

            lastCommit = RepositoryCommit(msg,displayName, timestamp)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}