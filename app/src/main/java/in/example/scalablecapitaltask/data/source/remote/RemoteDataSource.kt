package `in`.example.scalablecapitaltask.data.source.remote

import `in`.example.scalablecapitaltask.data.models.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.logging.Level
import java.util.logging.Logger


class RemoteDataSource {

    private fun getData(url: URL): String? {
        var connection: HttpURLConnection? = null
        try {
            connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.connect()
            val status = connection.responseCode

            when (status) {
                200, 201 -> {
                    val br = BufferedReader(InputStreamReader(connection.inputStream))
                    val sb = StringBuilder()
                    var line = br.readLine()
                    while (line != null) {
                        sb.append(line + "\n")
                        line = br.readLine()
                    }
                    br.close()
                    return sb.toString()
                }
            }

        } catch (e: MalformedURLException) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE, null, e)
        } catch (e: IOException) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE, null, e)
        } finally {
            if (connection != null) {
                try {
                    connection.disconnect()
                } catch (ex: Exception) {
                    Logger.getLogger(javaClass.name).log(Level.SEVERE, null, ex)
                }

            }
        }
        return null
    }

    fun getRepositoryData(userId: String, pageNum: Int): List<GithubRepository>? {
        val requestType = RequestType.repository
        val jsonString = getData(requestType.getUrl(userId, pageNum.toString()))
        if (jsonString != null) {
            val repositoryApiResponse = RepositoryApiResponse(jsonString)
            return repositoryApiResponse.reposList
        }
        return null
    }

    fun getLastRepositoryCommit(loginId: String, repoId: String): RepositoryCommit? {
        val requestType = RequestType.commit
        val jsonString = getData(requestType.getUrl(loginId, repoId))
        if(jsonString != null) {
            val commitResponse = CommitApiResponse(jsonString)
            return commitResponse.lastCommit
        }
        return null
    }

}