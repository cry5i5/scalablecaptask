package `in`.example.scalablecapitaltask.data.source

import `in`.example.scalablecapitaltask.data.models.GithubRepository
import `in`.example.scalablecapitaltask.data.models.RepositoryCommit
import `in`.example.scalablecapitaltask.data.source.local.LocalDataSource
import `in`.example.scalablecapitaltask.data.source.remote.RemoteDataSource
import android.content.Context


class DataRepository(context: Context) {

    val remoteDataSource: RemoteDataSource
    val localDataSource: LocalDataSource

    init {
        remoteDataSource = RemoteDataSource()
        localDataSource = LocalDataSource(context)
    }

    fun getRepositoryList(loginId: String, pageNum: Int): List<GithubRepository>? {
        var repoList: List<GithubRepository>? = localDataSource.getAllRepositories()
        if (repoList?.size == 0) {
            repoList = remoteDataSource.getRepositoryData(loginId, pageNum)
            if (repoList != null) {
                localDataSource.insertRepositories(repoList)
            }
        }
        return repoList
    }


    fun getLastCommit(loginId: String, repoId: String): RepositoryCommit? {
        return remoteDataSource.getLastRepositoryCommit(loginId, repoId)
    }
}