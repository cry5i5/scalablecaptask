package `in`.example.scalablecapitaltask.data.models

import org.json.JSONArray
import org.json.JSONException


class RepositoryApiResponse(jsonString: String) {

    var reposList: ArrayList<GithubRepository>? = null
    init {
        try {
            val jsonArray = JSONArray(jsonString)
            reposList = ArrayList()
            for (i in 0 until jsonArray.length()) {
                val repoObj = jsonArray.getJSONObject(i)
                val id = repoObj.getLong("id")
                val repoName = repoObj.getString("name")

                val repo = GithubRepository(id, repoName)
                reposList!!.add(repo)

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}