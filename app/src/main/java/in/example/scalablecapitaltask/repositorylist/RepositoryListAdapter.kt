package `in`.example.scalablecapitaltask.repositorylist

import `in`.example.scalablecapitaltask.R
import `in`.example.scalablecapitaltask.data.models.GithubRepository
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView



class RepositoryListAdapter(val context: Context?): RecyclerView.Adapter<RepositoryListAdapter.RepoViewHolder>() {

    var repoList = ArrayList<GithubRepository>()
    lateinit var listener: OnListClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.repo_row, parent, false)
        return RepoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return repoList.size
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        val repo = repoList[position]
        holder.repoTitle.text = repo.name
        val lastCommitMsg: String
        if(repo.lastCommit == null) {
            lastCommitMsg = "loading..."
        } else {
            lastCommitMsg = "Last Commit - ${repo.lastCommit!!.message} by ${repo.lastCommit!!
            .authorDisplayName}"
        }
        holder.commitMessage.text = lastCommitMsg

    }

    fun updateList(list: List<GithubRepository>) {
        repoList.clear()
        repoList.addAll(list)
        notifyDataSetChanged()
    }

    inner class RepoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val repoTitle = itemView.findViewById<TextView>(R.id.repo_title)
        val commitMessage = itemView.findViewById<TextView>(R.id.last_commit)
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            listener.clickOnPos(adapterPosition)
        }
    }

    interface OnListClickListener {
        fun clickOnPos(position: Int)
    }
}