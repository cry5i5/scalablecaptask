package `in`.example.scalablecapitaltask.repositorylist

import `in`.example.scalablecapitaltask.R
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast

class RepositoryListActivity : AppCompatActivity() {

    val THREAD_KEY = "THREAD_KEY"

    val LOAD_REPO_MSG = 100
    val LOAD_LAST_COMMITS = 101
    val ERROR_MSG = 102
    val UPDATE_REPO_LIST = 103

    lateinit var mHandlerThread: HandlerThread
    lateinit var mBackgroundHandler: Handler
    lateinit var mUIHandler: Handler

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: RepositoryListAdapter

    private var mViewModel: RepositoryListViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mHandlerThread = HandlerThread(THREAD_KEY)
        mHandlerThread.start()


        mUIHandler = Handler(object : Handler.Callback {
            override fun handleMessage(p0: Message?): Boolean {
                if (p0 != null) {
                    when (p0.what) {
                        ERROR_MSG -> Toast.makeText(this@RepositoryListActivity,
                                "Something went wrong", Toast
                                .LENGTH_SHORT).show()
                        UPDATE_REPO_LIST -> {
                            mAdapter.updateList(mViewModel!!.repositoryList!!)
                        }
                    }
                }
                return true
            }
        })




        mBackgroundHandler = Handler(mHandlerThread.looper, object: Handler.Callback {

            override fun handleMessage(p0: Message?): Boolean {
                if(p0 != null) {
                    when (p0.what) {
                        LOAD_REPO_MSG -> loadUserRepository()
                        LOAD_LAST_COMMITS -> loadLastCommitOfAllRepo()
                    }
                }
                return true
            }
        })


        mRecyclerView = findViewById(R.id.list)
        val manager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mAdapter = RepositoryListAdapter(this)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.layoutManager = manager

        mViewModel = RepositoryListViewModel(this)




        val listClickListener = object : RepositoryListAdapter.OnListClickListener {
            override fun clickOnPos(position: Int) {

            }
        }
        mAdapter.listener = listClickListener

        mBackgroundHandler.sendMessage(Message().apply {
            what = LOAD_REPO_MSG
        })
    }

    private fun loadUserRepository() {
        mViewModel!!.getRepositoryList()
        if(mViewModel!!.repositoryList == null) {
            mUIHandler.sendEmptyMessage(ERROR_MSG)
        } else {
            mUIHandler.sendEmptyMessage(UPDATE_REPO_LIST)
            mBackgroundHandler.sendEmptyMessage(LOAD_LAST_COMMITS)
        }
    }

    private fun loadLastCommitOfAllRepo() {
        mViewModel!!.loadLastCommitOfRepos()
        mUIHandler.sendEmptyMessage(UPDATE_REPO_LIST)
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandlerThread.quit()
        mViewModel = null
    }
}
