package `in`.example.scalablecapitaltask.repositorylist

import `in`.example.scalablecapitaltask.data.models.GithubRepository
import `in`.example.scalablecapitaltask.data.source.DataRepository
import android.content.Context
import android.os.Looper


class RepositoryListViewModel(context: Context) {
    private var dataRepository: DataRepository

    var repositoryList: List<GithubRepository>? = null

    init {
        dataRepository = DataRepository(context)
    }


    fun getRepositoryList(pageNum: Int = 0) {
        if(Looper.myLooper() == Looper.getMainLooper()) {
            throw RuntimeException("getRepositoryList must not be called on main thread")
        }
        repositoryList = dataRepository.getRepositoryList("mralexgray", pageNum)
    }

    fun loadLastCommitOfRepos() {
        if(Looper.myLooper() == Looper.getMainLooper()) {
            throw RuntimeException("loadLastCommitOfRepos must not be called on main thread")
        }
        for(repo in repositoryList!!) {
            val lastCommit = dataRepository.getLastCommit("mralexgray", repo.name)
            repo.lastCommit = lastCommit
        }
    }
}